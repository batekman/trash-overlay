# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Original Author: Thomas Fischer <fischer@unix-ag.uni-kl.de>
# Purpose: From a specified table of use flags, download urls, file patterns,
# and licenses, build variables such as SRC_URI and LICENSE and functions for
# installation.

# Expected format of a table:
# - Columns are separated by pipe symbols ("|")
# - There are four columns as follows:
#   1. a single use flag as to be used for IUSE
#   2. the download url pointing to an archive (e.g. a .tar.gz file)
#   3. pattern which files to be installed
#      (avoid spaces and other tricky characters)
#   4. the license applying to this specific download,
#      for use in LICENSE variable
#
# Example from media-fonts/openfontlibrary:
#  serif|http://openfontlibrary.org/assets/downloads/judson/51d83ce369280539578dd360d378d814/judson.zip|Judson-*.ttf|OFL
# would download judson.zip and install all files matching Judson-*.ttf
# if the use flag "serif" is set and the license "OFL" is not excluded



# Extract all download urls from the given table
# but put a conditional use flag in front of it.
# One line per url, which may look like:
#   serif? ( http://openfontlibrary.org/78d814/judson.zip )
function multiplesources_src_uri {
	local TABLE="$1"
	local useflag
	local url
	local filepattern
	local license

	for line in ${TABLE} ; do
		read useflag url filepattern license <<<${line//|/ }
		echo "${useflag}? ( ${url} )"
	done
}

# Extract a list of file patterns to be installed.
# List is filtered by use flags, i.e. only for downloads
# enabled by a use flag the pattern will be printed.
# For example, if use flag "serif" is set, one line may be
#   Judson-*.ttf
function multiplesources_filepatterns {
	local TABLE="$1"
	local useflag
	local url
	local filepattern
	local license

	for line in ${TABLE} ; do
		read useflag url filepattern license <<<${line//|/ }
		if use ${useflag} ; then echo "${filepattern}" ; fi
	done
}

# Extract a list of licenses of the files to get installed.
# List is filtered by use flags, i.e. only for downloads
# enabled by a use flag the license will be printed.
# For example, if use flag "serif" is set, one line may be
#   OFL
# There is no filtering for duplicates; if you want to have
# this, add "| sort -u" after the for loop's done.
function multiplesources_licenses {
	local TABLE="$1"
	local useflag
	local url
	local filepattern
	local license

	for line in ${TABLE} ; do
		read useflag url filepattern license <<<${line//|/ }
		if use ${useflag} ; then echo "${license}" ; fi
	done
}

