# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit qt4-r2 git-r3

DESCRIPTION="Play any media files, convert, extract and more"
HOMEPAGE="https://github.com/rupeshs/ExMplayer"
EGIT_REPO_URI="https://github.com/rupeshs/ExMplayer.git"

EGIT_COMMIT="v${PV}"

LICENSE="GPL-2 LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="virtual/ffmpeg
		dev-qt/qtcore:4
		dev-qt/qtgui:4
		dev-qt/qtdbus:4"
RDEPEND="${DEPEND}
		media-video/mplayer"

src_install() {
	install -Dm755 "${S}/src/exmplayer" "${D}/usr/bin/exmplayer"

	install -dm755 "${D}/usr/share/applications"
	install -m644 "exmplayer.desktop" "exmplayer_enqueue.desktop" "${D}/usr/share/applications"
	install -Dm644 "debian/exmplayer.png" "${D}/usr/share/pixmaps/exmplayer.png"

	install -dm755 "${D}/etc/exmplayer"
	install -m644 linux_build/{sc_default.xml,fmts} "${D}/etc/exmplayer"

	install -dm755 "${D}/usr/share/exmplayer"
	# use native installed ffmpeg
	ln -s /usr/bin/ffmpeg  "${D}/usr/share/%{name}/ffmpeg"
	# or bundled ffmpeg
	#install -m755 linux_build/ffmpeg %{buildroot}/usr/share/%{name}

}
