# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit qmake-utils git-2

DESCRIPTION="iStodo - organizer for students"
HOMEPAGE="http://istodo.ru/"
EGIT_REPO_URI="https://bitbucket.org/istodo/istodo-desktop.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="dev-qt/qtcore:5
		dev-qt/qtsql:5"

RDEPEND="${DEPEND}"

src_prepare() {
    sed -i '/\s\+iOS$/d' iStodo.pro
}

src_compile() {
	eqmake5 .
	emake
}

src_install() {
	dobin desktop/iStodo
}
