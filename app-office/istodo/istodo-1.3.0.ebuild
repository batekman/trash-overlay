# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit qmake-utils

DESCRIPTION="iStodo - organizer for students"
HOMEPAGE="http://istodo.ru/"
SRC_URI="http://dev.istodo.ru/istodo-desktop/get/v1.3.0.tar.bz2"

RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="dev-qt/qtcore:5
		dev-qt/qtsql:5"

RDEPEND="${DEPEND}"

S="${WORKDIR}/istodo-istodo-desktop-adec47e4f41e"

src_compile() {
	eqmake5 .
	emake
}

src_install() {
	dobin desktop/iStodo
}
