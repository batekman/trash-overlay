# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit cmake-utils

DESCRIPTION="Tiny text translator - Google Translate GUI"
HOMEPAGE="http://flareguner.github.io/litetran/"
SRC_URI="https://github.com/flareguner/${PN}/archive/${PV}.tar.gz -> ${PN}-${PV}.tar.gz"

RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=">=dev-util/cmake-2.8.10
        dev-qt/qtcore:5
        dev-qt/qtgui:5
        dev-qt/linguist-tools:5
        dev-qt/qtmultimedia:5
        dev-qt/qtx11extras:5
        dev-qt/qtwidgets:5"

RDEPEND="${DEPEND}"
