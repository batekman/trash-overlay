# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils git-2

DESCRIPTION="Perl script to convert FLAC files to MP3 format."
HOMEPAGE="http://robinbowes.github.io/flac2mp3/"
RESTRICT="mirror"
EGIT_REPO_URI="https://github.com/robinbowes/flac2mp3.git"
EGIT_BRANCH="master"

LICENSE="as-is"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="dev-perl/Audio-FLAC-Header
		 dev-perl/Text-Glob
		 dev-perl/MP3-Tag
		 dev-perl/Number-Compare
		 dev-perl/File-Find-Rule
		 dev-perl/Proc-ParallelLoop
		 dev-perl/File-Which"

src_install() {
	newbin flac2mp3.pl flac2mp3
	dodoc *.txt
}
