# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils font

# where to install font files
INSTALLDIR="/usr/share/fonts/${PN}"

DESCRIPTION="Fonts shipped with Adobe Reader made available"
HOMEPAGE="http://www.adobe.com/products/reader/"
SRC_URI=""

RESTRICT="mirror"

LICENSE="Adobe"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="=app-text/acroread-9*"
DEPEND="${RDEPEND}"

S="${WORKDIR}"

src_install() {
	insinto ${INSTALLDIR}

	# install all OTF files from Adobe Reader 9's font directory
	doins /opt/Adobe/Reader9/Resource/Font/*.otf \
	   || die "cannot install fonts"

	font_xfont_config
	font_fontconfig
}
