# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils font multiplesources

RESTRICT="mirror"

# where to install font files
INSTALLDIR="/usr/share/fonts/${PN}"

DESCRIPTION="Fonts that come with the freedom to use, study, share and remix"
HOMEPAGE="http://openfontlibrary.org/"

# Structure of this variable:
# - in each line, encode one font from OpenFontLibrary
# - fields are separated by |
# - fields are as follows:
#   - use flag (see IUSE below)
#   - download url (from OpenFontLibrary's page for font)
#   - pattern which files to be installed
#     (.otf files are preferred over .ttf)
#   - license (Open Font License in most cases,
#     see OpenFontLibrary's page for font)
FONTLIST="serif|http://openfontlibrary.org/assets/downloads/kunkhmer/f88152d740424e3cb2f4357cbd8d470c/kunkhmer.zip|*.ttf|LGPL-3
          handwriting|http://openfontlibrary.org/assets/downloads/kool-korean/b6658573e07887ee44c8f3eab9690122/kool-korean.zip|*.ttf|CCPL-Attribution-ShareAlike-3.0
          sansserif|http://openfontlibrary.org/assets/downloads/stop-motion--/8800d4124e2a4370239e4444707dc8d4/stop-motion--.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/junction/f99fa7793abd572cc4e202f8cafa1a28/junction.zip|*/*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/sansus-webissimo/9e7ef959f7aeef22383039a04e56def9/sansus-webissimo.zip|*.otf|CCPL-Attribution-3.0
          sansserif|http://openfontlibrary.org/assets/downloads/grana-padano/67c44957a5109298272b45dd1fb7c616/grana-padano.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/dataface/9f377ae1b625f598e36a32ecbca042c6/dataface.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/lateef/4ec4808dc3f67cce4357c468b0f14f3a/lateef.zip|*/*.ttf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/futhark-adapted/5c02df4a239c746506a3acdb36f815ad/futhark-adapted.zip|*.ttf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/consolamono/7140a3309f96fc07aa7fa2e04d4841de/consolamono.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/osp-din/fa3d08bd4b819d71da421ae73c1bd1d1/osp-din.zip|*/*.ttf|ofl
          dingbat|http://openfontlibrary.org/assets/downloads/socialbats/8fd6a1320345eb057c09016e6e704411/socialbats.zip|*/*/*.ttf|ofl
          blackletter|http://openfontlibrary.org/assets/downloads/babacar/cb7658969d402bab642a87cc67cf3550/babacar.zip|*/*.otf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/asakim/147953fe481073aca3e206e361fd7a00/asakim.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/crimson/6feb6f6187adb04e0ea4a40f69101d98/crimson.zip|*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/khmerosclassic/d1f8870c713447285a0a1823a872b5fc/khmerosclassic.zip|*.ttf|LGPL-3
          handwriting|http://openfontlibrary.org/assets/downloads/certege-italic/0d7b7d50088242f15128fbd5ed3fa5a4/certege-italic.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/on-the-wall/c504a5af0d518ad086c8acdad4cdf421/on-the-wall.zip|*.otf|ofl
          dingbat|http://openfontlibrary.org/assets/downloads/fivefoldornamentsetc/a55fac4c07165cf675a4834a4a7d1d61/fivefoldornamentsetc.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/lertica/5df1273a283df6ceb13a4b766e14cde6/lertica.zip|*/*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/sabatica/37397f5aa588fb0e54c89c3251e83dcf/sabatica.zip|*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/megrim/7c7ebbace2b20afe253743b779da3044/megrim.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/cursive/5cebcce9dcab068da6d414fc8cbe4dd6/cursive.zip|*/*.ttf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/friendship-code/f4b93b79a5890ab323e0aa7f3392a6cc/friendship-code.zip|*.ttf|CCPL-Attribution-ShareAlike-3.0
          serif|http://openfontlibrary.org/assets/downloads/squareantiqua/9e7334002d839460e2d6cb7fe9f217b0/squareantiqua.zip|*/*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/ramasuri/9df5e09fb8709db054560bf5fe286b7a/ramasuri.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/pfennig/8b5fa73ca4cf4cfa42d21b9f73f6060b/pfennig.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/rohingya-kuna-leyka-noories/05450a0c39acdbdb8f143ec2bd2415a8/rohingya-kuna-leyka-noories.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/classica/c71db1e55d9c49832b5f2589283c863b/classica.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/jura/a19f536a40d8911d72ca770d9aa79be9/jura.zip|*.ttf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/notcouriersans/283f31e5facea102ba05ffe4d60b340f/notcouriersans.zip|*/*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/hanumanb/e163c1333e34c943c1910a20a6cae9a3/hanumanb.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/wirewyrm/518fb61f72ec00041aefb117247406b9/wirewyrm.zip|*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/amiri/72f2b510b00e53e1995efa7c1d2728c1/amiri.zip|*/*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/averia-sans/43cb788084feccb8f62f13624cfcb156/averia-sans.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/hilo-deco/429821b7032eeedf314c99f1f1cd0cb2/hilo-deco.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/eadui/f7b140ad037eb65f4e1d3ba9d8a36515/eadui.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/de-puntillas-a-lace-regular/7d3849404273392bfd35cd32c396fae3/de-puntillas-a-lace-regular.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/averia/dc61396d0f8e3b0130933b1227022c20/averia.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/strato/8f48bd68e7bb4e4bcb167478853b263d/strato.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/alegreya/447ea3a5e998d7381aa58124a8f385c7/alegreya.zip|*/*/*.otf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/gnutypewriter/8a7cb57723df9125503083ed59e7a11c/gnutypewriter.zip|*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/univers-else/0b1af07228dee086b7b4cff30d58c316/univers-else.zip|*/*.ttf|ofl
          dingbat|http://openfontlibrary.org/assets/downloads/richstyle/5ae945dbd9da1ab01c247ee1d5574e9e/richstyle.zip|*/*.ttf|CCPL-Attribution-3.0
          sansserif|http://openfontlibrary.org/assets/downloads/medievalsharp/968442122196c5d363e8d51244632966/medievalsharp.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/open-baskerville/7d908608fdbf11a2f990dee983c3ef35/open-baskerville.zip|*/*.otf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/miri/7919e9b95d2ce590cfc561f369e4085d/miri.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/douar-outline/08717079a1f176808971879f24d44f39/douar-outline.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/acknowledgement/a5294ce75f3058111216b845e2bd2a6d/acknowledgement.zip|*/*/*.otf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/funky-fuji/7a894d56f326c4817812e0833445e931/funky-fuji.zip|*.ttf|CCPL-Attribution-ShareAlike-3.0
          sansserif|http://openfontlibrary.org/assets/downloads/cousine/e64962b5515c2e41b8cd473d0113be51/cousine.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/kh-kangrey/658f9b0d445695f429444136c54bee0e/kh-kangrey.zip|*.ttf|LGPL-3
          display|http://openfontlibrary.org/assets/downloads/de-puntillas-f-tiptoes-squid-bd/8e22d8ac5ea9d3efc6903be17346939d/de-puntillas-f-tiptoes-squid-bd.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/curlyhogrunes/4859fa3382fe75c99b56bc768410c477/curlyhogrunes.zip|*/*.ttf|GPL-3
          display|http://openfontlibrary.org/assets/downloads/de-puntillas-c-lace-lt/31a45d12acc21178e7368318dccffad7/de-puntillas-c-lace-lt.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/paskol/629faa9080ae7e109cf0e7bffe93e979/paskol.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/graziano/493b3e1535054a8f076d02ba9f945ad1/graziano.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/freeuniversal/5077bdf47767ac210dd15ea83870df66/freeuniversal.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/ponyo/474ed7369b7091c34516c159a8012c1f/ponyo.zip|*.otf|ofl
          dingbat|http://openfontlibrary.org/assets/downloads/xecret/176d57ba63d5d66fc78e6e8411422b7c/xecret.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/flaggothic/658cec249f3ef4aad26ca7b5c59c5370/flaggothic.zip|*/*.ttf|ofl
          blackletter|http://openfontlibrary.org/assets/downloads/just-letters/8c227e1dc6d780804e08b3182a5a2e8b/just-letters.zip|*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/vegesignes/b8daaa9b460167f2c59099c631f9f3e1/vegesignes.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/logisoso/d94347fc6166204bec37fcf8f9b43e40/logisoso.zip|*/*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/dark-garden/eda15a9eebe0e98b807551f1299f68de/dark-garden.zip|*.ttf|GPL-3
          sansserif|http://openfontlibrary.org/assets/downloads/cholerik/09ba09c02fba57b9f6c13ec11ee94436/cholerik.zip|*/*.otf|ofl
          dingbat|http://openfontlibrary.org/assets/downloads/goddesssymbols/f2f584ceb55804acc99633462e6ac50e/goddesssymbols.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/terminal-grotesque/762705a385b7c63050671bfab84cbcb0/terminal-grotesque.zip|*/*.ttf|CCPL-Attribution-3.0
          handwriting|http://openfontlibrary.org/assets/downloads/intuitive/71763f72bef454e1fc6ac6ddc2fd8e7f/intuitive.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/didact-gothic/e12a3678deb5791ac79cf0e3f0569663/didact-gothic.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/la-chata/0e36b00d348f2ce6086a9d1b284f87f3/la-chata.zip|*/*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/keenton/ec91ba7bf9ea7ca86075f896edf31e93/keenton.zip|*/*.otf|CCPL-Attribution-3.0
          dingbat|http://openfontlibrary.org/assets/downloads/knots/77dde10f170ca063908c554afdcb43a9/knots.zip|*/*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/anarchy-sans/07268d8ee5db545584da4cb36dceb62e/anarchy-sans.zip|*.otf|CCPL-Attribution-3.0
          display|http://openfontlibrary.org/assets/downloads/pugsley/9352beb9fec5a385c61535e4516f4e3e/pugsley.zip|*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/kh-chrieng-metal/bdbf568bad6fb20dbb971d3336a613d3/kh-chrieng-metal.zip|*.ttf|LGPL-3
          dingbat|http://openfontlibrary.org/assets/downloads/unicons/9d0fb3d288fbff1b894d4d1c8bb4c972/unicons.zip|*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/backout/b5c0b88eedcc0459c3e6ef6f4bdeb5fb/backout.zip|*/*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/vds/d65c97ecc931195595b8dda41844233a/vds.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/ahuramazda/4e0eff7f1abbcbc533333a41fd38f5f3/ahuramazda.zip|*/*.ttf|ofl
          monospaced|http://openfontlibrary.org/assets/downloads/banana-brick/2100182bf265504c3dc286eccb6383a9/banana-brick.zip|*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/nova/9fea9bf34ffdc4d4c44a8502d0deb44b/nova.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/designosaur/20b76920b181bc400c45166473687ed6/designosaur.zip|*/*.otf|CCPL-Attribution-3.0
          serif|http://openfontlibrary.org/assets/downloads/rohingya-gonya-leyka-noories/10b5c4174f506b7ad25ebbb6b4c1e1c9/rohingya-gonya-leyka-noories.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/pecita/1075c97afa1497d982d3e7d6c424c225/pecita.zip|*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/tnua-libre/a3bd7e43878f1d3deb87369d8d06706c/tnua-libre.zip|*.ttf|GPL-3
          dingbat|http://openfontlibrary.org/assets/downloads/web-symbols/26570e66618b46bd347b580e043282f4/web-symbols.zip|*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/averia-serif/51e3ae7cf61b932c613416e6e0a05e40/averia-serif.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/lets-trace/2bda9d1771d50bdbac12553291852b53/lets-trace.zip|*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/serreria-sobria/0fa0545f2a578458f054e16ebb9b7942/serreria-sobria.zip|*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/gentium/ce99cf60f293aba5294c7554dc1a7b16/gentium.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/a-bebedera/fee3c61026c4f848cfd29fc6301607fd/a-bebedera.zip|*.otf|ofl
          blackletter|http://openfontlibrary.org/assets/downloads/gamaliel/e0c9601992c81cd08b8d449411bc0ac6/gamaliel.zip|*/*/*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/neocyr/ab264f5199213ec554b0eaa11ce18286/neocyr.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/klaudia-and-berenika/0a3b87bd1a161792058e644854ee7b9b/klaudia-and-berenika.zip|*/*/*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/dotrice/8bbaa7a763470e5a042344fd2a6a9907/dotrice.zip|*/*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/nemoy/a3347504a711e41edd8de2d5d7312b6f/nemoy.zip|*/*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/brivido/9597a29c71cdf5f6f0117f76ce9f6e10/brivido.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/roundstyle/d2ec2b9af785938972a4785eb9abcee2/roundstyle.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/kmsbbicsf/86cce945431a4b424877d165cc6a14e8/kmsbbicsf.zip|*.ttf|LGPL-3
          serif|http://openfontlibrary.org/assets/downloads/judson/50f9a241a5e2b8e46f0297e1067242c7/judson.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/nocurvesboustrophedon/1d06925436946fdcce5d9d29977d03e8/nocurvesboustrophedon.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/de-puntillas-d-to-tiptoe-lt/0e83f5eddd4d4a2bb54fe525b03d005f/de-puntillas-d-to-tiptoe-lt.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/modernantiqua/62d1ce486a50b18919fc60879e67ee9a/modernantiqua.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/orthoventional/808028d42ebc9a2201c48ec793cd7860/orthoventional.zip|*.otf|ofl
          display|http://openfontlibrary.org/assets/downloads/onilesca/1089ce821e1e638c1299713c86e23130/onilesca.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/libertinage/7e850a8dd5040bf3070dc8a720604844/libertinage.zip|*/*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/niharika/cd7c413bed26593f311ea105651b0aab/niharika.zip|*.ttf|GPL-3
          serif|http://openfontlibrary.org/assets/downloads/old-standard/8f7616da5d24d2c54ec2d8e734320ac1/old-standard.zip|*/*.otf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/serreria-extravagante/df170769ea0b76fbb06a55d97e7971e6/serreria-extravagante.zip|*.otf|ofl
          serif|http://openfontlibrary.org/assets/downloads/pierce/2c9409aaecfcadc09f38ff3bd5d4638d/pierce.zip|*.otf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/nehama/d61b6d564b289aa0a7d10e25a2961088/nehama.zip|*.ttf|ofl
          serif|http://openfontlibrary.org/assets/downloads/school-cursive/4e370890e2f7a7b7f6f0edb88ce65e72/school-cursive.zip|*.ttf|MIT
          sansserif|http://openfontlibrary.org/assets/downloads/alfphabet/55201027210b27c9c647eb66918f3abe/alfphabet.zip|*/*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/migdal-haemeq/12bbcbce60e7c7b3b1670ea4eb2eb956/migdal-haemeq.zip|*.ttf|ofl
          handwriting|http://openfontlibrary.org/assets/downloads/dragon/4aaed2b0163f0318a56dd8f720017f18/dragon.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/news-cycle/d16d9c8311c84dd3d03841e9606a0b0d/news-cycle.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/retro-perspective/f2f5fc70ff2aadeb47495c89c0155846/retro-perspective.zip|*.ttf|ofl
          blackletter|http://openfontlibrary.org/assets/downloads/triod-postnaja/064a178a89d1ff5a304797c08b75fa76/triod-postnaja.zip|*.ttf|ofl
          sansserif|http://openfontlibrary.org/assets/downloads/polsku-regula/ba39b59e25ff577d8f4ae8c20139cd12/polsku-regula.zip|*/*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/de-puntillas-b-to-tiptoe-regular/227bfb785daca2360811f1345aff3ef3/de-puntillas-b-to-tiptoe-regular.zip|*.ttf|ofl
          display|http://openfontlibrary.org/assets/downloads/de-puntillas-e-tiptoes-squid/582ff7bc6d0a4d7ca2e64b4b21600839/de-puntillas-e-tiptoes-squid.zip|*.ttf|ofl"

SRC_URI=$(multiplesources_src_uri "${FONTLIST}")
LICENSE=$(multiplesources_licenses "${FONTLIST}")

SLOT="0"
KEYWORDS="~x86"

IUSE="blackletter dingbat display handwriting +monospaced +sansserif +serif"

S="${WORKDIR}"

src_install() {
	insinto ${INSTALLDIR}

	# install all font files as listed in the pattern list
	doins $(multiplesources_filepatterns "${FONTLIST}" )

	font_xfont_config
	font_fontconfig
}

