# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit font

DESCRIPTION="Microsoft's Tahoma font"
HOMEPAGE="http://microsoft.com"
SRC_URI="tahoma.ttf tahomabd.ttf"
RESTRICT="fetch mirror"
LICENSE="MSttfEULA"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~x86-fbsd"

S=${WORKDIR}
FONT_S=${WORKDIR}
FONT_SUFFIX="ttf"
FONTDIR="/usr/share/fonts/tahoma"
MYDISTDIR="/usr/portage/distfiles"

src_unpack() {
	cp ${DISTDIR}/tahoma.ttf ${DISTDIR}/tahomabd.ttf ${S}
}
