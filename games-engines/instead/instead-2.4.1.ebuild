# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit cmake-utils games

DESCRIPTION="INSTEAD quest engine"
HOMEPAGE="http://instead.syscall.ru/"
SRC_URI="mirror://sourceforge/${PN}/${PN}_${PV}.tar.gz"

RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="=dev-lang/lua-5.1*
   media-libs/libsdl
   media-libs/sdl-mixer
   media-libs/sdl-image
   media-libs/sdl-ttf"
RDEPEND="${DEPEND}"

src_configure() {
  mycmakeargs=(
    -DCMAKE_INSTALL_PREFIX=""
    -DBINDIR=${GAMES_BINDIR}
    -DDATADIR=${GAMES_DATADIR}/${PN}
    -DSHAREDIR=${GAMES_DATADIR}/${PN}
    -DDOCDIR=${GAMES_DATADIR}/doc/${PN}
    -DICONDIR=/usr/share/pixmaps
    -DMANDIR=/usr/share/man
    -DDESKTOPDIR=/usr/share/applications
    )
   cmake-utils_src_configure
}

src_compile() {
   cmake-utils_src_compile
}

src_install() {
   cmake-utils_src_install
}
