# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
inherit vim-plugin eutils git-2

DESCRIPTION="Easy-peasy todo list manager for VIM"
HOMEPAGE="https://github.com/codegram/vim-todo"
LICENSE="vim"
KEYWORDS=""
EGIT_REPO_URI="https://github.com/codegram/vim-todo"
