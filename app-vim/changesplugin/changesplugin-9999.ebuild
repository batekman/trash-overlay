# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
inherit vim-plugin eutils git-2

DESCRIPTION="A Vim Plugin for indicating changes as colored bars using signs."
HOMEPAGE="https://github.com/chrisbra/changesPlugin"
LICENSE="vim"
KEYWORDS=""
EGIT_REPO_URI="https://github.com/chrisbra/changesPlugin"
