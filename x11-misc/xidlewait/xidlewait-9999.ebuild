# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils git-2

DESCRIPTION="Fork of xprintidle which exits when idle time reaches the specified"
HOMEPAGE="https://github.com/batekman/xidlewait"
RESTRICT="mirror"
EGIT_REPO_URI="https://github.com/batekman/xidlewait.git"
EGIT_BRANCH="master"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="x11-base/xorg-server
	|| ( app-arch/lzma-utils app-arch/xz-utils )"
RDEPEND="x11-base/xorg-server"

src_configure() {
    econf \
        --x-libraries=/usr/lib/X11 \
        --x-includes=/usr/include/X11
}

src_install() {
	einstall
}
