# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

DESCRIPTION="Zoomable user interface with plugin applications"
HOMEPAGE="http://eaglemode.sourceforge.net/"

SRC_URI="http://prdownloads.sourceforge.net/${PN}/${P}.tar.bz2"
RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="0"

KEYWORDS="~x86 ~amd64"

IUSE="jpeg png tiff xine svg pdf freetype \
	7z ar arj bzip2 gzip lha lzo rar xz zip unzip zoo tar \
	abiword dia dvi transfig netpbm html iso povray \
	rpm wmf"

DEPEND=">=dev-lang/perl-5.8
	x11-libs/libX11
	jpeg? ( virtual/jpeg )
	png? ( media-libs/libpng )
	tiff? ( media-libs/tiff )
	xine? ( media-libs/xine-lib )
	freetype? ( media-libs/freetype )
	svg? ( gnome-base/librsvg )
	pdf? ( app-text/poppler )
	7z? ( app-arch/p7zip )
	ar? ( sys-devel/binutils )
	arj? ( app-arch/arj )
	bzip2? ( app-arch/bzip2 )
	gzip? ( app-arch/gzip )
	lha? ( app-arch/lha )
	lzo? ( app-arch/lzop )
	tar? ( app-arch/tar )
	abiword? ( app-office/abiword )
	dia? ( app-office/dia )
	dvi? ( app-text/dvipsk )
	transfig? ( media-gfx/transfig )
	netpbm? ( media-libs/netpbm )
	html? ( app-text/htmldoc )
	iso? ( app-cdr/cdrtools )
	povray? ( media-gfx/povray )
	rpm? ( app-arch/rpm )
	wmf? ( media-libs/libwmf )"

RDEPEND="${DEPEND}
	x11-terms/xterm
	app-text/ghostscript-gpl"


make_pl_buildargs() {
	echo "continue=no"
	# make sure we don't try to build modules that need build-time libs
	if ! (use jpeg && use pdf && use png && use svg && use tiff && use xine); then
		echo "projects=not:$(\
			use jpeg || echo -n emJpeg,;\
			use png || echo -n emPng,;\
			use tiff || echo -n emTiff,;\
			use xine || echo -n emAv,;\
			use pdf || echo -n emPdf,;\
			use svg || echo -n emSvg)"
	fi
}

src_compile() {
	local cpus
	if has_version dev-lang/perl[ithreads] ; then
		einfo "Building with mutliple CPU cores"
		cpus=$(makeopts_jobs)
	else
		einfo "Perl not built with threads support, using 1 CPU core"
		cpus=1
	fi
	perl make.pl build cpus="${cpus}" $(make_pl_buildargs) || die "Compilation failed"
	# perl make.pl build continue=no || die "build failed"
}

# src_install() {
# 	perl make.pl install "root=${D}" "dir=/usr/lib/eaglemode" menu=yes bin=yes \
# 	|| die "install failed"
# }
src_install() {
	# TODO multilib
	perl make.pl install "root=${D}" "dir=/usr/lib/eaglemode" \
		menu=yes bin=yes || die "Installation failed"

	dodoc README
	dosym /usr/lib/eaglemode/doc/ /usr/share/doc/${PF}/doc
}

pkg_postinst() {
	elog "Eaglemode can use many optional programs at runtime"
	elog "to display and process different kinds of files."
	elog "For a list of these optional programs see"
	elog "/usr/share/doc/${PF}/doc/html/SystemRequirements.html"
}
