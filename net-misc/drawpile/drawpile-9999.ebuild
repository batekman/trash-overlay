# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit cmake-utils git-2

DESCRIPTION="Networking drawing (whiteboarding) program"
HOMEPAGE="http://drawpile.net/"
EGIT_REPO_URI="git://github.com/callaa/Drawpile"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="client server gif"

REQUIRED_USE="|| ( client server )"

DEPEND="dev-qt/qtnetwork:5
		kde-frameworks/karchive:5
		client? (
			dev-qt/qtgui:5
			dev-qt/qtcore:5
			dev-qt/qtxml:5
			dev-qt/qtconcurrent:5
			dev-qt/qtmultimedia:5
			dev-qt/qtsvg:5
			dev-qt/qtwidgets:5
			dev-qt/qtdeclarative:5
		)
		gif? ( media-libs/giflib )"

RDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
		$(cmake-utils_use client CLIENT)
		$(cmake-utils_use server SERVER)
	)

	cmake-utils_src_configure
}
